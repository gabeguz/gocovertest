package main

import (
	"fmt"

	"gitlab.com/gabeguz/gocovertest/greetings"
	"rsc.io/quote"
)

func main() {
	fmt.Printf("I say %q and %q\n", quote.Hello(), greetings.Goodbye())
	TestUnitCoverage()
}

func TestUnitCoverage() {
	fmt.Printf("Test Unit")
}
